local function show_popup_panel(title, popup_text)
  local vb = renoise.ViewBuilder()
    renoise.app():show_custom_dialog(
    title, 

    vb:column {
      margin = renoise.ViewBuilder.DEFAULT_DIALOG_MARGIN,
      spacing = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING,
      uniform = true,
  vb:text {
    text = popup_text  
  } 

    }
  )
  
end

--------------------------------------------------------------------------------
-- GUI
--------------------------------------------------------------------------------
--[[


What options are we offering?

- Skip these lines: some set of line number values
    Could use the pattern generator from Alternator
    This would apply rand_remove(odds) to lines not matching this set
    
- Keep only these lines: some set of line number values
    Could use the pattern generator from Alternator
    This removes all notes except for the given lines

- rand_remove only these lines: some set of line number values
    Could use the pattern generator from Alternator
    This would apply rand_remove(odds) only to lines matching this set

- Remove only these lines: some set of line number values
    Could use the pattern generator from Alternator
    This removes all notes matching the given lines

Then the option to apply only to selected track pattern, or to
the whole track.  For now, just work on selected track.

So there are two things happening:  Line selection, and what operation to apply

If the line-number list is empty then operate on all lines.

We can start with the Alternator gui, omit the shifting part, keep the fields
for the operator and the generated values.

Add radio boxes (?) for

Only these lines? ()
Skip these lines  ()

BTW, if you wanted to just remove all lines matching selection, just set probability to 100.

So add a field for probability 

--]]


GUI = {}

GUI.dialog = nil
GUI.vb = {}
GUI.current_text = ""
GUI.lines_list = ""


GUI.values = nil
GUI.tool_name  = ""
GUI.tool_id   = ""
GUI.tool_version   = ""

GUI.probability_value_text = ""
GUI.func_text = ""
GUI.note_off_text = ""

GUI.spacing = 4 * renoise.ViewBuilder.DEFAULT_CONTROL_SPACING
GUI.margin  = 1.5 * renoise.ViewBuilder.DEFAULT_DIALOG_MARGIN

GUI.help_text = [[ 
The goal of this tool is to remove some percentage of notes from a note column.

There are two factors: a given probability, and a list of line numbers.

You need to enter the probablity as a number between 0 and 100.   
While there is little point in using 0%, it is allowed.

If you want to remove all notes that match a list of line numbers just set the probabilty to 100.

You can enter line numbers by hand, but you can also use a list generator function.

This generator understands a very simple and limited syntax:

`+ i j k` means line numbers increment by i, then j, then k, then i, then j, etc.


For example: 

`+ 3 5` would give you `0 3 8 11 16 19 24 ...` up to the number to lines in the current pattern.

 `+ 3 2 5` would give you `0 3 5 10 13 15 20 23 25  ...` up to the number to lines in the current pattern.


`/ i j k` gives you all the lines evenly divisible by any of i, j, k, etc.

For example: 

`/ 3 5` would give you `0 3 5 6 9 10 12 15 18 20 21 ...` up to the number to lines in the current pattern.

`/ 6` would give you `0 6 12 18 24 30 ...` up to the number to lines in the current pattern.

Please note that there must be spaces between the characters in the function.

You can edit the resulting list.  Do not use commas in either the function or the line-number list.

Each time you run a line-generation function the generated numbers are *added* to the current list. 

This allows you to construct a more complex sequence by combining different line-generation functions.

There is a `clear` button to clear the current list; you can also hand-edit that list to fine-tune the values.

You must click the `Go` button in order to apply the results.

The last-applied values are saved and reloaded the next time the tool is used.

Note: Clearing the entire line number list will be treated as wanting to act on all lines.

A bit unintuitive, but having no lines is pointless, and it saves you the trouble of having to fill the list.


  ]]


-- ======================================================================
function line_nums_to_table(lines_str)
  print("In line_nums_to_table")
  local nums_table = string.int_list_to_numeric(lines_str)
  local paired_table = {}

  for k,v in pairs(nums_table ) do
    paired_table[v] = v
  end

  return paired_table 
end

-- ======================================================================
function load_values()
  print("Load last-used values ...")

  GUI.values = renoise.Document.create("ErodeLastValues") {
    gen_function        = GUI.func_text,
    lines_list          = GUI.lines_list,
    lines_negation      = GUI.lines_negation or false,
    note_off            = GUI.note_off or false,
    probability_value   = GUI.probability_value_text,

  }

  local ok, err = GUI.values:load_from("erode.xml")
  print("Loaded previous values. err = " , err)

  GUI.func_text               = GUI.values.gen_function.value 
  GUI.lines_list              = GUI.values.lines_list.value  
  GUI.lines_negation          = GUI.values.lines_negation.value
  GUI.note_off                = GUI.values.note_off.value
  GUI.probability_value_text  = GUI.values.probability_value.value


  print("Loaded values, GUI.func_text = " .. "'" .. GUI.func_text .. "'")

end

-- ======================================================================
function save_values()
  print("Save current values ...")

  if GUI.values ~= nil then
    GUI.values.gen_function.value = GUI.func_text
    
    GUI.values.probability_value.value = GUI.probability_value_text
    GUI.values.lines_list.value  = GUI.vb.views.lines_list_field.text
    GUI.values.lines_negation.value  = GUI.vb.views.lines_negation.value
    GUI.values.note_off.value  = GUI.vb.views.note_off.value
    -- TODO: Add a field for negation; i.e, operate only lines that are *not* in the line set
    GUI.values:save_as("erode.xml")
  end
end



--	function  GUI.rotate_values_left()
--	  print("Roate values left")
--	  GUI.vb.views.probability_value_text.text = rotate_left(GUI.vb.views.probability_value_text.text)
--	  GUI.probability_value_text = GUI.vb.views.probability_value_text.text 
--	end
--	
--	
--	function  GUI.rotate_values_right()
--	  print("Roate values right")
--	  GUI.vb.views.probability_value_text.text = rotate_right(GUI.vb.views.probability_value_text.text)
--	  GUI.probability_value_text = GUI.vb.views.probability_value_text.text 
--	end

-- TODO Decide how to define 'class' functions.  Either use `Foo.func_name = function` or `function Foo.func_name`


-- ======================================================================
GUI.show_dialog = function()

  if GUI.dialog and GUI.dialog.visible then
    GUI.dialog:show()
    return
  else
    GUI.dialog = {}
  end

  GUI.vb = renoise.ViewBuilder()
  GUI.current_text = ""  

  load_values()

  
 local button_help = GUI.vb:button {
      text = "Help",
      width = 40,
      released = function()
        show_popup_panel("Help", GUI.help_text )
      end
    }
 
 
--	  local button_rotate_left = GUI.vb:button {
--	      text = " << ",
--	      released = function()
--	        GUI.rotate_values_left()
--	      end
--	    }
--	
--	
--	
--	  local button_rotate_right = GUI.vb:button {
--	      text = " >> ",
--	      released = function()
--	        GUI.rotate_values_right()
--	      end
--	    }
--	

  local title = GUI.tool_name .. ". Version " .. GUI.tool_version

  local fx_prompt_row1 = GUI.vb:row {
    GUI.vb:text {
      text = "This is the Erode tool.\nIt clears notes in a given set of track-pattern lines based on a given probability\n\nYou must have a note column selected for this to work.",
     },
    }

  local fx_prompt_row2 = GUI.vb:row {
    GUI.vb:text {
      text = "By default it simply clears the note, but there is an option to insert 'OFF' instead.\nLine negation tells the tool to erode only lines that are not in the line list.\n\nClick the Help button for more details.",
     },
    }

        -- print("What is GUI.lines_negation: " .. GUI.lines_negation)
  local line_negation_row = GUI.vb:row {

  GUI.vb:checkbox {


   value = GUI.lines_negation or false,
   id = "lines_negation",
   notifier = function()
     -- HERE
     print("GUI.vb.views.lines_negation clicked")
      GUI.lines_negation = GUI.vb.views.lines_negation.value
        print("Updated GUI.lines_negation: " , GUI.lines_negation) -- The ".." is concatenation
      
   end
},

    GUI.vb:text {
        text = "Use negation of line numbers",
        tooltip = "Skip instead of apply-to",
      },

 }

    -- ************************ -- 
  local note_off_row = GUI.vb:row {

  GUI.vb:checkbox {


   value = GUI.note_off or false,
   id = "note_off",
   notifier = function()
     -- HERE
     print("GUI.vb.views.note_off clicked")
      GUI.note_off = GUI.vb.views.note_off.value
        print("Updated GUI.note_off: " , GUI.note_off) -- The ".." is concatenation
      
   end
},

    GUI.vb:text {
        text = "Use note off instead of clearing the current value",
        tooltip = "Insert note off",
      },

 }
    -- ************************ -- 


  local function_row = GUI.vb:row {
    GUI.vb:text {
      text = "Line selection function",
      tooltip = "Function to generate line numbers",
    },

    GUI.vb:textfield {
      text = GUI.func_text,
      id = "func_text",
      notifier = function()
        GUI.func_text = string.upper(GUI.vb.views.func_text.text)
        print("Updated GUI.func_text: " .. GUI.func_text)
      end,
    },
    GUI.vb:button {
      text = "Generate",
      released = function()
        GUI.generate_values()
      end
    } 
  } -- end of row 


  local probability_value_row = GUI.vb:row {
    GUI.vb:text {
      text = "Probability (0 to 100)",
      tooltip = "Likelihood of removal",
    },

    -- button_rotate_left,

    GUI.vb:textfield {
      text = GUI.probability_value_text,
      id = "probability_value_text",
      notifier = function()
        GUI.probability_value_text = string.upper(GUI.vb.views.probability_value_text.text)
        print("Updated GUI.probability_value_text: " .. GUI.probability_value_text)
      end,
    },


    -- GUI.vb.views.lines_negation.value,
  } -- end of row 



  local button_clear = GUI.vb:button {
    text = "Clear",
    released = function()
      GUI.vb.views.lines_list_field.text = ""
    end
  } 

  local button_go = GUI.vb:button {
    text = "Go",
    width = 40,
    released = function()
      Core.erode(GUI)
      save_values()
      GUI.dialog:close()
    end
  }


  local prompts_row = GUI.vb:row {
          GUI.vb:column {
        spacing = GUI.spacing,
        margin = GUI.margin,

    fx_prompt_row1,
    fx_prompt_row2,
   },
  }

  local bottom_buttons_row = GUI.vb:row {
  
      GUI.vb:horizontal_aligner {
      width = "100%",
      
        mode = "justify",
      spacing = 320,
        button_go, 
        button_help,
      },

} -- end of row 


  -- ********************************** ROW 
  local content = GUI.vb:column {

    prompts_row, 

    GUI.vb:column {
      spacing = GUI.spacing,
      margin = GUI.margin,
      probability_value_row ,
      function_row,
    },

    GUI.vb:column {
      spacing = GUI.spacing,
      margin = GUI.margin,

      GUI.vb:text {
        text = "Apply to lines:",
        tooltip = "Series of line numbers",
      },

      GUI.vb:multiline_textfield {
        id = "lines_list_field",
        width = 400,
        height = 50,
        text = GUI.lines_list,
        notifier = function()
          GUI.lines_list = GUI.vb.views.lines_list_field.text
        end
      },

      GUI.vb:horizontal_aligner {
        mode = "right",
        button_clear,
      },


      line_negation_row,
      note_off_row,
      bottom_buttons_row,


    },

  } -- end of main row


  GUI.dialog = renoise.app():show_custom_dialog(title, content)  

end


-- ======================================================================
GUI.generate_values = function() 

  print("DEBUG: generate_values begin.") -- JGB DEBUG
  local current_set = line_nums_to_table(GUI.vb.views.lines_list_field.text)
  -- Need to turn this into a table, then generate new values, then merge the tables, sort, and remove dupes

  local function_str = string.trim(GUI.func_text)

  if U.is_empty_string(function_str) then
    print("Function string is empty, returning.")
    return 
  end

    print("DEBUG: function string is ", function_str) -- JGB DEBUG
  -- Need to get the new list and populate the text box

  local new_set = Core.new_set_from_funct_string(function_str, current_set)
  print("New, generated, set: ")

  -- DEBUG WE get here, but new_set is empty :(

  rprint(new_set)

  GUI.vb.views.lines_list_field.text = table.concat(new_set, " ")
  GUI.lines_list  = GUI.vb.views.lines_list_field.text
end

return GUI

