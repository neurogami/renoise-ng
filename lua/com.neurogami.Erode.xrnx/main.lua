--[[======================================================
com.neurogami.Erode.xrnx/main.lua
=======================================================]]--


-- TODO: Needs a GUI so you can set some values, such as percentage of notes to remove

RENOISE_OSC    = nil
CONTROLLER_OSC = nil

TOOL_NAME = "Erode"
U  = require (TOOL_NAME .. '/Utilities')

require (TOOL_NAME .. '/Core')
require (TOOL_NAME .. '/Gui')


-- Reload the script whendever this file is saved. 
_AUTO_RELOAD_DEBUG = true

local function clamp_value(value, min_value, max_value)
  return math.min(max_value, math.max(value, min_value))
end



class "RenoiseScriptingTool" (renoise.Document.DocumentNode)
  function RenoiseScriptingTool:__init()    
    renoise.Document.DocumentNode.__init(self) 
    self:add_property("Name", "Untitled Tool")
    self:add_property("Id", "Unknown Id") 
    self:add_property("Version", "Unknown version") 
  end

local manifest = RenoiseScriptingTool()
local ok,err = manifest:load_from("manifest.xml")
GUI.tool_name = manifest:property("Name").value
GUI.tool_id = manifest:property("Id").value
GUI.tool_version = manifest:property("Version").value

print("Alternator has version: " .. GUI.tool_version)



local function alternator_gui()
  GUI.current_text = ""
  GUI.show_dialog() 
end



function erode_pattern_track()
  local s = renoise.song()
  --   local bpm = s.transport.bpm 
  --   s.transport.bpm = clamp_value(bpm+1, 32, 999)
end



function erode_entire_track()
  local s = renoise.song()
  --   local bpm = s.transport.bpm 
  --   s.transport.bpm = clamp_value(bpm-1, 32, 999)
end


function erode_options()
  GUI.show_dialog() 
end

renoise.tool():add_menu_entry {
  name = "--- Main Menu:Tools:Neurogami " .. TOOL_NAME .. ": Options ...",
  invoke = erode_options
}

renoise.tool():add_menu_entry {
  name = "--- Main Menu:Tools:Neurogami " .. TOOL_NAME .. ": Up " .. TOOL_NAME .. "  ..",
  invoke = erode_pattern_track
}


renoise.tool():add_menu_entry {
  name = "--- Main Menu:Tools:Neurogami " .. TOOL_NAME .. ": Down " .. TOOL_NAME .. "  ..",
  invoke = erode_entire_track
}

------- keys --------------------------

renoise.tool():add_keybinding {
  name = "Global:Tools:Neurogami " .. TOOL_NAME .. " Erode options dialog ..",
  invoke = erode_options
}  


renoise.tool():add_keybinding {
  name = "Global:Tools:Neurogami " .. TOOL_NAME .. " Erode current track pattern ..",
  invoke = erode_pattern_track
}  


renoise.tool():add_keybinding {
  name = "Global:Tools:Neurogami " .. TOOL_NAME .. " Erode entire track..",
  invoke = erode_entire_track
}  


